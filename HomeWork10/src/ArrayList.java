

public class ArrayList implements List {
    private static final int DEFAULT_SIZE = 10;

    private int[] elements;

    private int size;

    public ArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.size = 0;
    }

    @Override
    public void add(int element) {
        if (size == elements.length) {
            resize();
        }
        this.elements[size] = element;
        size++;
    }

    private void resize() {
        int[] newElements = new int[elements.length + elements.length / 2];
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }
        this.elements = newElements;
    }

    @Override
    public boolean contains(int element) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }


    public void removeByIndex(int index) {
        if (index >= 0 && index < size) {
            System.arraycopy(elements, index + 1, elements, index, size - index);
            size--;
        }
    }


    @Override
    public void remove(int element) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == element) {
                System.arraycopy(elements, i + 1, elements, i, size - i);
                break;
            }
        }
        size--;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < size) {
            return elements[index];
        } else {
            System.err.println("Out of bounds");
            return -1;
        }

    }

    @Override
    public void addFirst(int element) {
        if (size == elements.length) {
            resize();
        }
        for (int i = size + 1; i > 0; i--) {
            this.elements[i] = this.elements[i - 1];
        }
        this.elements[0] = element;
        size++;
    }
}