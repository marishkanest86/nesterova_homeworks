public class Main {

    public static void main(String[] args) {
        List list = new ArrayList();
        list.add(8);
        list.add(11);
        list.add(-3);
        list.add(28);
        list.add(5);
        list.add(11);
        list.add(-3);
        list.add(28);
        list.add(5);
        list.add(-3);
        list.add(28);
        list.add(5);

        list.remove(11);

        System.out.println(list.contains(-3));
        System.out.println(list.contains(17));

        System.out.println(list.get(3));
        System.out.println(list.get(10));

    }
}
