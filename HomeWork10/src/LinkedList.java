public class LinkedList implements List {

    private static class Node {


        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;
    private int size;

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        size++;
    }

    @Override
    public void addFirst(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            add(element);
        } else newNode.next = first;
        first = newNode;
        size++;
    }

    @Override
    public boolean contains(int element) {
        for (Node current = first; current != null; current = current.next) {
            if (current.value == element) {
                return true;
            }
        }
        return false;
    }


    @Override
    public int size() {
        return size;
    }

    public void removeByIndex(int index) {
        if (index >= 0 && index < size) {
            for (Node current = first, prev = null; current != null; prev = current, current = current.next) {
                if (current == first) {
                    first = current.next;
                    current.next = null;
                } else if (current == last) {
                    prev.next = null;
                    last = prev;
                } else {
                    prev.next = current.next;
                    current.next = null;
                }
                size--;
            }
        }
    }



    public void remove(int element) {
        for (Node current = first, prev = null; current != null; prev = current, current = current.next) {
            if (current.value == element) {
                if (current == first) {
                    first = current.next;
                    current.next = null;
                } else if (current == last) {
                    prev.next = null;
                    last = prev;
                } else {
                    prev.next = current.next;
                    current.next = null;
                }
                size--;
            }
        }
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < size) {
            Node current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        System.err.println("index out of bounds");
        return -1;
    }

}
