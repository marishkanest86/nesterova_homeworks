package ru.inno;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {

        final List<String> strings = Files.readAllLines(
                Paths.get("cars.txt"));

        strings.forEach(System.out::println);
        System.out.println(("---------------------------"));

        final List<String[]> stringArrays = strings.stream().map(string -> string.split(",")).collect(Collectors.toList());

        stringArrays.forEach((elem) -> System.out.println(Arrays.toString(elem)));
        System.out.println("--------------------------------");

        stringArrays.stream()
                .filter(record -> record[2].equals("Black") || record[3].equals("0"))
                .map(record -> record[0])
                .forEach(System.out::println);

        System.out.println("--------------------------------");

        stringArrays.stream()
                .filter(record -> Integer.parseInt(record[4]) > 700000 && Integer.parseInt(record[4]) < 800000)
                .map(record -> record[1])
                .distinct()
                .forEach(System.out::println);

        System.out.println("--------------------------------");

        System.out.println(stringArrays.stream()
                .min(Comparator.comparing((record) -> Integer.parseInt(record[4])))
                .map(record -> record[2]).get());

        System.out.println(stringArrays.stream()
                .filter(record -> record[1].equals("Toyota"))
                .mapToInt(record -> Integer.parseInt(record[4])).average());

    }
}
