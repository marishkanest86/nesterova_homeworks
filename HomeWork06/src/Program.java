public class Program {
    private String nameOfProgram;

    public Program(String nameOfProgram) {
        this.nameOfProgram = nameOfProgram;
    }

    public String getNameOfProgram() {
        return nameOfProgram;
    }
}

