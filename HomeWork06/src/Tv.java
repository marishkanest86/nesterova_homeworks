public class Tv {

    private Channel[] channels;
    private static final int MAX_CHANNELS_COUNT = 3;
    private int channelsCount;
    public Tv() {
        this.channels = new Channel[MAX_CHANNELS_COUNT];
    }

    public void addChannel(Channel channel) {
        if (channelsCount < MAX_CHANNELS_COUNT) {
            channels[channelsCount] = channel;
            channelsCount++;
        }
    }
    public void showChannel (int number) {
        if (number < 0 || number >= channelsCount) {
            System.err.println( " Канала под номером [" + number + "] не существует");
        }
        Channel channel = channels[number];
        channel.randomProgram();

    }

}
