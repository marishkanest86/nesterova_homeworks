public class Main {

    public static void main(String[] args) {
        Channel channel = new Channel("1", 5);
        Channel channel1 = new Channel("2", 5);
        Channel channel2 = new Channel("3", 5);

        Program program = new Program("Орел и Решка");
        Program program1 = new Program("Паддингтон");
        Program program2 = new Program("News");
        Program program3 = new Program("News2");
        Program program4 = new Program("Оранжевая корова");
        Program program5 = new Program("Зебра в клеточку");

        channel.addProgram(program);
        channel.addProgram(program1);

        channel1.addProgram(program2);
        channel1.addProgram(program3);

        channel2.addProgram(program4);
        channel2.addProgram(program5);


        Tv tv = new Tv();
        tv.addChannel(channel);
        tv.addChannel(channel1);
        tv.addChannel(channel2);



        RemoteController remoteController = new RemoteController(tv);
        remoteController.showChannel(2);
        remoteController.showChannel(4);
    }
}
