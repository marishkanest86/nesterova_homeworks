public class RemoteController {
    private Tv tv;

    public RemoteController(Tv tv) {
        this.tv = tv;
    }

    public void setTv(Tv tv) {
        this.tv = tv;
    }


    public void showChannel(int number) {
        tv.showChannel(number);
    }

}
