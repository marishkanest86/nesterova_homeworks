import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Channel {

    private String nameOfChannel;
    private Program[] programs;

    private int programsCount;
    private int initialCapacity;

    public Channel(String nameOfChannel) {
        this(nameOfChannel, 5);
    }

    public Channel(String nameOfChannel, int initialCapacity) {
        this.nameOfChannel = nameOfChannel;
        this.initialCapacity = initialCapacity;
        this.programs = new Program[this.initialCapacity];
    }

    public void addProgram(Program program) {
        if (programsCount >= initialCapacity) {
            initialCapacity *= 2;
            programs = Arrays.copyOf(programs, initialCapacity);
        }
        programs[programsCount++] = program;
    }

    public void randomProgram() {
        int index = ThreadLocalRandom.current().nextInt(programsCount);
        System.out.println(programs[index].getNameOfProgram());
    }

}
