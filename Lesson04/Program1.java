import java.util.Scanner;
class Program1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int currentNumber = scanner.nextInt();
		boolean isPowerOfTwo = isPowerOfTwo(currentNumber);
		if (isPowerOfTwo){
			System.out.println (currentNumber + "- yes");
		}
		else {
			System.out.println (currentNumber + "- no");
		}
	}	
	public static boolean isPowerOfTwo (int currentNumber){
		if (currentNumber == 2) {
			return true;
		}
		else if (currentNumber % 2 != 0) {
				return false;
		}
		return isPowerOfTwo (currentNumber/2);
	}
}
