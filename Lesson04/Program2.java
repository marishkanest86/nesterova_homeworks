import java.util.Scanner;
class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numberForSearch = scanner.nextInt();
		int array[] = {-10,-5,2,5,10,15,20};
		
		int isExist = binarySearch(array,numberForSearch);
		if (isExist <0){
			System.out.println (numberForSearch + "- no");
		}
		else {
			System.out.println (numberForSearch + "- yes " + isExist + " - position in array" );
		}
	}	
	public static int binarySearch (int array[], int numberForSearch){
		int left = 0;
		int right = array.length-1;
		return binarySearchRec (array, numberForSearch, left, right);
		}

	public static int binarySearchRec (int array[], int numberForSearch, int left, int right){
        int middle;
        middle = (left + right) / 2;
        if (numberForSearch > array[middle]){
        	left = middle + 1;
        }
        else {
        	right = middle-1;
        }
		if (array[middle] == numberForSearch){
			return middle;
		}
		else {
			if (left <= right) {
				binarySearchRec (array, numberForSearch, left, right);
			}
			else {
				return -1;
			}
		}
		return binarySearchRec (array, numberForSearch, left, right);
	}
}