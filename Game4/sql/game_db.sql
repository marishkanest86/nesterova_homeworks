create table player
(
    id         serial primary key,
    ip         varchar,
    nickname   varchar,
    points     varchar,
    winsCount  integer,
    losesCount integer
);

create table game
(
    id                     serial primary key,
    dateTime               timestamp,
    secondsGameTimeAmount  bigint,
    playerFirstShotsCount  integer,
    playerSecondShotsCount integer,
    playerFirst_id         integer,
    playerSecond_id        integer,
    foreign key (playerFirst_id) references player (id),
    foreign key (playerSecond_id) references player (id)
);

create table shot
(
    id       serial primary key,
    dateTime timestamp,
    game_id  integer,
    foreign key (game_id) references game (id),
    shooter  integer,
    foreign key (shooter) references player (id),
    target   integer,
    foreign key (target) references player (id)
);