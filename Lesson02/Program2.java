import java.util.Scanner;
import java.util.Arrays;
class Program2{
	public static void main(String[] args) {
		Scanner scanner  = new Scanner (System.in);
		int n =scanner.nextInt();
		int array [] = new int[n];
		for (int i=0; i<array.length; i++) {
			array[i] = scanner.nextInt();
		}
		int min = array[0];
		int max = array [0];
		int positionOfMin = 0;
		int positionOfMax = 0;
		for (int i = 1; i < array.length; i++) {
			if (array [i] < min) {
				min = array [i];
				positionOfMin = i;
			}
		}
		for (int i = 1; i < array.length; i++) {
			if (array [i] > max) {
				max = array [i];
				positionOfMax = i;
			}
		}
			int temp = array [positionOfMin];
			array [positionOfMin] = array [positionOfMax];
			array [positionOfMax] = temp;
			System.out.println ( Arrays.toString (array));
	}
}