import java.util.Scanner;
import java.util.Arrays;
class Program1{
	public static void main(String[] args) {
		Scanner scanner  = new Scanner (System.in);
		int n =scanner.nextInt();
		int array [] = new int[n];
		for (int i=0; i<array.length; i++) {
			array[i] = scanner.nextInt();
		}
		for (int i = 0, j= array.length-1; i < j; i++,j--) {
			int temp = array [i];
			array [i] = array [array.length-1-i];
			array [array.length-1-i] = temp;
		}
		System.out.println ( Arrays.toString (array));
	}
}