class Program1 {
	public static void main(String[] args) {
		int number = 12345;
		int digitSum = 0;
		while (number !=0) {
			int lastDigit = number % 10;
			digitSum = digitSum + lastDigit;
			number = number / 10;
		}
		System.out.println(digitSum);
	}
}
