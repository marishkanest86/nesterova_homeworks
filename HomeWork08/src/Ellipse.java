public  class Ellipse extends Figure {

    public Ellipse(Point center, double side1, double side2) {
        super(center, side1, side2);
    }

    public Ellipse(double side1, double side2) {
        super(side1, side2);
    }

    public double perimeter() {
        return 4 * (((Math.PI * side1 * side2) + Math.pow(side1 - side2, 2)) / side1 + side2);

    }

    public double square() {
        return Math.PI * side1 * side2;
    }

}
