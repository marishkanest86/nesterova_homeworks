public abstract class Figure implements Movable, Scalable {
    Point center;
    protected double side1;
    protected double side2;


    public Figure(Point center, double side1, double side2) {
        this.center = center;
        this.side1 = side1;
        this.side2 = side2;
    }

    public Figure(double side1, double side2) {
        this(new Point(), side1, side2);
    }

    public Point getCenter() {
        return center;
    }

    public double getSide1() {
        return side1;
    }

    public double getSide2() {
        return side2;
    }

    public abstract double perimeter();

    public abstract double square();


    public void move(double x, double y) {
        center.setX(x);
        center.setY(y);
    }


    public void move(Point point) {
        move(point.getX(), point.getY());
    }


    public void setScale(double scale) {
        this.side1 = side1 * scale;
        this.side2 = side2 * scale;
    }

    public String toString() {
        return "Figure{" + center + "," + side1 + "," + side2 + ",perimeter=" + perimeter() + ", square=" + square() + "}";
    }
}
