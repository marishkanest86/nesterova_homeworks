public class Circle extends Ellipse {
    public Circle(Point center, double radius) {
        super(center, radius, radius);
    }

    public Circle(double radius) {
        super(radius, radius);
    }

}
