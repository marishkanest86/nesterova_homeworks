import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Figure rectangle = new Rectangle(20, 30);
        Figure square = new Square(10);

        Figure ellipse = new Ellipse(20, 30);
        Figure circle = new Circle(10);
        List<Figure> figures = Arrays.asList(rectangle, square, ellipse, circle);

        for (Figure figure : figures) {
            System.out.println(figure);
            figure.setScale(2.0);
            figure.move(2.0, 3);
            System.out.println(figure);

        }
    }
}
