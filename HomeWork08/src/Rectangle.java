public class Rectangle extends Figure {
    public Rectangle (Point center, double side1, double  side2){
        super (center, side1, side2);
    }
    public Rectangle (double side1, double  side2){
        this ( new Point(),side1, side2);
    }

    public double perimeter () {
        return 2* (side1+side2);
    }

    public double square () {
        return side1*side2;
    }
}
