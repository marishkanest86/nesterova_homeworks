package ru.inno.models;

import java.time.LocalDateTime;
import java.util.Objects;

public class Shot {
    private Long id;
    private LocalDateTime dateTime;
    private Game game;
    private Player shooter;
    private Player target;

    public Shot(Long id, LocalDateTime dateTime, Game game, Player shooter, Player target) {
        this.id = id;
        this.dateTime = dateTime;
        this.game = game;
        this.shooter = shooter;
        this.target = target;
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Game getGame() {
        return game;
    }

    public Player getShooter() {
        return shooter;
    }

    public Player getTarget() {
        return target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shot shot = (Shot) o;
        return Objects.equals(id, shot.id) && Objects.equals(dateTime, shot.dateTime) && Objects.equals(game, shot.game) && Objects.equals(shooter, shot.shooter) && Objects.equals(target, shot.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateTime, game, shooter, target);
    }
}
