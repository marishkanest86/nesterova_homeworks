package ru.inno.models;

import java.util.Objects;

public class Player {
    private Long id;
    private String ip;
    private String nickname;
    private Integer points;
    private Integer winsCount;
    private Integer losesCount;

    public Player(Long id, String ip, String nickname, Integer points, Integer winsCount, Integer losesCount) {
        this.id = id;
        this.ip = ip;
        this.nickname = nickname;
        this.points = points;
        this.winsCount = winsCount;
        this.losesCount = losesCount;
    }

    public Long getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }

    public String getNickname() {
        return nickname;
    }

    public Integer getPoints() {
        return points;
    }

    public Integer getWinsCount() {
        return winsCount;
    }

    public Integer getLosesCount() {
        return losesCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(id, player.id) && Objects.equals(ip, player.ip) && Objects.equals(nickname, player.nickname) && Objects.equals(points, player.points) && Objects.equals(winsCount, player.winsCount) && Objects.equals(losesCount, player.losesCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ip, nickname, points, winsCount, losesCount);
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", ip='" + ip + '\'' +
                ", nickname='" + nickname + '\'' +
                ", points=" + points +
                ", winsCount=" + winsCount +
                ", losesCount=" + losesCount +
                '}';
    }
}
