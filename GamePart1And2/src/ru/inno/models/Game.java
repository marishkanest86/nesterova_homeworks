package ru.inno.models;

import java.time.LocalDateTime;
import java.util.Objects;

public class Game {
    private Long gameId;
    private LocalDateTime dateTime;
    private Player playerFirst;
    private Player playerSecond;
    private Integer playerFirstShotsCount;
    private Integer playerSecondShotsCount;
    private Long secondsGameTimeAmount;

    public Game(Long gameId, LocalDateTime dateTime, Player playerFirst, Player playerSecond, Integer playerFirstShotsCount, Integer playerSecondShotsCount, Long secondsGameTimeAmount) {
        this.gameId = gameId;
        this.dateTime = dateTime;
        this.playerFirst = playerFirst;
        this.playerSecond = playerSecond;
        this.playerFirstShotsCount = playerFirstShotsCount;
        this.playerSecondShotsCount = playerSecondShotsCount;
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    public Long getGameId() {
        return gameId;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Player getPlayerFirst() {
        return playerFirst;
    }

    public Player getPlayerSecond() {
        return playerSecond;
    }

    public Integer getPlayerFirstShotsCount() {
        return playerFirstShotsCount;
    }

    public Integer getPlayerSecondShotsCount() {
        return playerSecondShotsCount;
    }

    public Long getSecondsGameTimeAmount() {
        return secondsGameTimeAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(gameId, game.gameId) && Objects.equals(dateTime, game.dateTime) && Objects.equals(playerFirst, game.playerFirst) && Objects.equals(playerSecond, game.playerSecond) && Objects.equals(playerFirstShotsCount, game.playerFirstShotsCount) && Objects.equals(playerSecondShotsCount, game.playerSecondShotsCount) && Objects.equals(secondsGameTimeAmount, game.secondsGameTimeAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameId, dateTime, playerFirst, playerSecond, playerFirstShotsCount, playerSecondShotsCount, secondsGameTimeAmount);
    }
}

