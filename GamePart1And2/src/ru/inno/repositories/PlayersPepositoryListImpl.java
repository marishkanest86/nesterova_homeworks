package ru.inno.repositories;

import ru.inno.models.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PlayersPepositoryListImpl implements PlayersPepository {

    private List<Player> players;

    public PlayersPepositoryListImpl() {
        this.players =new  ArrayList<>();
    }


    @Override
    public Optional<Player> findByNickname(String nickname) {
        for (Player player:players){
            if (player.getNickname().equals(nickname));
            return Optional.of(player);
        }
        return Optional.empty();
    }

    @Override
    public void save(Player player) {
        players.add (player);

    }

    @Override
    public void update(Player player) {
        players.set(players.indexOf(player),player);
    }
}
