package ru.inno.repositories;

import ru.inno.models.Game;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GamesRepositoryListImpl implements GamesRepository {

    private List<Game> games;

    public GamesRepositoryListImpl() {
        this.games = new ArrayList<>();

    }

    @Override
    public void save(Game game) {
        games.add (game);
    }

    @Override
    public Optional<Game> getById(Long gameId) {
        for (Game game : games) {
            if (game.getGameId().equals(gameId)) ;
            return Optional.of(game);
        }
        return Optional.empty();
    }

    @Override
    public void update(Game game) {
        games.set(games.indexOf(game),game);
    }
}
