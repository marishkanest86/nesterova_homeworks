package ru.inno.repositories;

import ru.inno.models.Game;

import java.util.Optional;

public interface GamesRepository {

    void save (Game game);

    Optional<Game> getById (Long gameId);

    void update (Game game);

}
