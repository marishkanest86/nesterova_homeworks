package ru.inno.repositories;

import ru.inno.models.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PlayersPepositoryMapImpl implements PlayersPepository {


    private Map<String,Player> players;

    public PlayersPepositoryMapImpl() {
        this.players = new HashMap<>();
    }

    @Override
    public Optional<Player> findByNickname(String nickname) {
        return Optional.ofNullable(players.get(nickname));
    }

    @Override
    public void save(Player player) {
        this.players.put(player.getNickname(),player);

    }

    @Override
    public void update(Player player) {
        this.players.put(player.getNickname(),player);
    }
}
