package ru.inno.repositories;

import ru.inno.models.Player;

import java.util.Optional;

public interface PlayersPepository {

    Optional<Player> findByNickname (String nickname);

    void save(Player player);

    void update (Player player);

}
