package ru.inno.repositories;

import ru.inno.models.Game;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class GamesRepositoryMapImpl implements GamesRepository {

    private Map<Long, Game> games;

    public GamesRepositoryMapImpl() {
        this.games = new HashMap<>();
    }

    @Override
    public void save(Game game) {
        this.games.put(game.getGameId(), game);
    }

    @Override
    public Optional<Game> getById(Long gameId) {
        return Optional.ofNullable(games.get(gameId));
    }

    @Override
    public void update(Game game) {
        this.games.put(game.getGameId(), game);
    }

}
