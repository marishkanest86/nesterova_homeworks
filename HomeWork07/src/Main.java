public class Main {

    public static void main(String[] args) {
        User user1 = User.builder()
                .setFirstName("Marsel")
                .setLastName("Sidikov")
                .setAge(27)
                .setIsWorker(true)
                .build();

        User user2 = User.builder()
                .setFirstName("Marina")
                .setAge(33)
                .build();

        System.out.println(user1.toString());
        System.out.println(user2.toString());

    }
}
