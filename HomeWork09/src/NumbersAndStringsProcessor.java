import java.util.Arrays;

public class NumbersAndStringsProcessor {

    private String[]  stringArray;
    private int[] intArray;

    public NumbersAndStringsProcessor(String[] stringArray, int[] intArray) {
        this.stringArray = stringArray;
        this.intArray = intArray;
    }

    String[] process (StringProcess process) {
        String[] strings = Arrays.copyOf(stringArray,stringArray.length);
        for (int index=0; index < stringArray.length; index++){
            strings[index] = process.process(stringArray[index]);
        }
        return strings;
    }
    int [] process (NumberProcess process) {
        int[] numbers = Arrays.copyOf(intArray,intArray.length);
        for (int index=0; index < numbers.length; index++){
            numbers [index] = process.process(intArray[index]);
        }
        return numbers;
    }
}
