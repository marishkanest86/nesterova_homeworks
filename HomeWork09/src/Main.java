import java.util.Arrays;

public class Main {

    static StringProcess reverseString = (string) -> new StringBuilder(string).reverse().toString();
    static StringProcess deleteNumberString = (string) -> string.replaceAll("\\d+", "");
    static StringProcess upperCaseString = (string) -> string.toUpperCase().toString();

    static NumberProcess deleteZerosNumber = (number)-> {
        String numberString = String.valueOf(number);
        String withoutZero=  numberString.replaceAll("0+","");
        return Integer.parseInt(withoutZero);
    };

    static NumberProcess reverseNumber = (number )-> {
        String numberString = String.valueOf(number);
        String reverseNumber = new StringBuilder(numberString).reverse().toString();
        return Integer.parseInt(reverseNumber);
    };

    static NumberProcess changeNumber = (number )-> {
        String numberString = String.valueOf(number);
        String changeNumber=  numberString.replaceAll("9","8").replaceAll("7","6").replaceAll("5","4").replaceAll("3","2").replaceAll("1","0");
        return Integer.parseInt(changeNumber);
    };



    public static void main(String[] args) {
        String[] strings = new String[]{"Kirill", "Albert", "Marina", "Ravil123", "Marina17"};
        int[] numbers = new int[]{2020, 17, 34, 1986, 26550};

        NumbersAndStringsProcessor numbersAndStringsProcessor = new NumbersAndStringsProcessor(strings, numbers);

        String[] process = numbersAndStringsProcessor.process(reverseString);
        System.out.println(Arrays.toString(process));

        String[] process1 = numbersAndStringsProcessor.process(deleteNumberString);
        System.out.println(Arrays.toString(process1));

        int[] process2 = numbersAndStringsProcessor.process(reverseNumber);
        System.out.println(Arrays.toString(process2));

        int[] process3 = numbersAndStringsProcessor.process(deleteZerosNumber);
        System.out.println(Arrays.toString(process3));

        String[] process4 = numbersAndStringsProcessor.process(upperCaseString);
        System.out.println(Arrays.toString(process4));

        int[] process5 = numbersAndStringsProcessor.process(changeNumber);
        System.out.println(Arrays.toString(process5));
    };


}
