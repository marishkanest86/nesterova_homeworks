import java.util.Scanner;
import java.util.Arrays;
class Program1 {
	
	public static int getSumOfDigitsFromArray (int array[]) {
		int number = 0;
		for (int i = 0; i < array.length; i++){
			number = number + array [i];
		}
		return number;	
	}
	public static int getNumberFromArray (int array[]) {
		int number = 0;
		for (int i = 0; i < array.length; i++){
			number = number * 10 + array [i];
		}
		return number;	
	}
	public static void rotateArray (int array[]){
		for (int i = 0, j= array.length-1; i < j; i++,j--) {
			int temp = array [i];
			array [i] = array [array.length-1-i];
			array [array.length-1-i] = temp;
		}	
	}
	public static double getAverage (int array[]) {
		double average = 0;
		if (array.length > 0){
			double sum = 0;
			for (int i = 0; i < array.length; i++){
				sum = sum + array[i];
			}
		average = sum / array.length;
		}
		return average;
	}
	public static void swapMaxAndMin (int array[]){
		int min = array[0];
		int max = array [0];
		int positionOfMin = 0;
		int positionOfMax = 0;
		for (int i = 1; i < array.length; i++) {
			if (array [i] < min) {
				min = array [i];
				positionOfMin = i;
			}
			if (array [i] > max) {
				max = array [i];
				positionOfMax = i;
			}	
		}

			int temp = array [positionOfMin];
			array [positionOfMin] = array [positionOfMax];
			array [positionOfMax] = temp;
	}
	public static void bubbleSort (int array[]){
		for (int i = 1; i < array.length; i++) {
			for (int j = 1; j < array.length-i-1; j++) {
				if (array[j] > array[j + 1]){
					int temp = array [j];
					array [j] = array[j + 1];
					array [j + 1] = temp;
				}
			}
		}
	}
	public static void main(String[] args) {
		Scanner scanner  = new Scanner (System.in);
		int n = scanner.nextInt();
		int array [] = new int[n];
		for (int i=0; i<array.length; i++) {
			array[i] = scanner.nextInt();
		}
		int sumOfDigitsFromArray = getSumOfDigitsFromArray (array);// задача 1.1 -сумма эоментов
		System.out.println (sumOfDigitsFromArray);
		double average = getAverage (array);//задача 1.3-среднеарифметическое элементов  массива
		System.out.println (average);		
		rotateArray (array);// задача 1.2 - разворот массива
		System.out.println ( Arrays.toString (array)); // печать массива
		swapMaxAndMin (array); // задача 1.4
		System.out.println ( Arrays.toString (array)); // печать массива 
		bubbleSort (array);// задача 1.5
		System.out.println ( Arrays.toString (array));
		int numberFromArray = getNumberFromArray (array);// задача 1.6 -  массив в число
		System.out.println (numberFromArray);
	}
}
